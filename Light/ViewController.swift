//
//  ViewController.swift
//  Light
//
//  Created by Gianluca Orpello on 01/12/2020.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var button: UIButton!
    
    var lightOn = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        button.setTitle("Toggle the light!!!", for: .normal)
    }

    @IBAction func buttonPressed(_ sender: UIButton) {
        lightOn.toggle()
        updateUI()
    }
    
    func updateUI() {
        if lightOn {
            view.backgroundColor = .white
            button.setTitle("OFF", for: .normal)
        }
        else {
            view.backgroundColor = .black
            button.setTitle("ON", for: .normal)
        }
    }
    
}

